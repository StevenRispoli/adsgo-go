package randCampaignData

var Pub = []string{
	"a.com",
	"b.com",
	"c.com",
	"d.com",
	"e.com",
	"f.com",
	"g.com",
	"h.com",
	"i.com",
	"j.com",
	"k.com",
	"l.com",
	"m.com",
	"n.com",
	"o.com",
	"p.com",
	"q.com",
	"r.com",
	"s.com",
	"t.com",
	"u.com",
	"v.com",
	"w.com",
	"x.com",
	"y.com",
	"z.com",
}

var Cat = []string{
	"IAB1",
	"IAB2",
	"IAB3",
	"IAB4",
	"IAB5",
	"IAB6",
	"IAB7",
	"IAB8",
	"IAB9",
	"IAB10",
	"IAB11",
	"IAB12",
	"IAB13",
	"IAB14",
	"IAB15",
	"IAB16",
	"IAB17",
	"IAB18",
	"IAB19",
	"IAB20",
	"IAB21",
	"IAB22",
	"IAB23",
	"IAB24",
	"IAB25",
	"IAB26",
}

var DeviceIDs = []int{
	3, 6, 7,
}

var IPAddress = []string{
	"2600:380:585d:a089:4d53:139b:e1ec:e17",
	"125.18.239.131",
	"5.68.75.17",
	"137.122.192.40",
}
