package main

import (
	"bytes"
	"encoding/json"
	"errors"
	// "fmt"
	"io"
	"reflect"
	"testing"
)

var (
	test_ip, test_user, test_country, test_region, test_city = "", "", "", "", ""
	test_one, test_two, test_three, test_four, test_five     = 1, 2, 3, 4, 5
)

//***Beginning of seat.go testing

//***Beginning of tests for BidReqValidate method on *bidReq

type data_BidReqValidate struct {
	bidReq                          *bidReq
	ip, user, country, region, city *string
	pubName                         string
}

type expected_BidReqValidate struct {
	ip, user, country, region, city *string
}

type test_BidReqValidate struct {
	data     data_BidReqValidate
	expected interface{}
}

var table_BidReqValidate = []test_BidReqValidate{
	{
		data: data_BidReqValidate{
			bidReq:  &bidReq{},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "Test request: Missing or empty ID",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "missingDeviceObject",
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "Test request: Missing device object",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID:     "missingUA",
				Device: &device{},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "Test request: Missing UA",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "missingSiteObject",
				Device: &device{
					UA: "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "Test request: Missing site object",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "missingDomainPage",
				Device: &device{
					UA: "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
				},
				Site: &site{},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "Test request: Missing domain and page info",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "missingIP",
				Device: &device{
					UA: "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
				},
				Site: &site{
					Page: "http://a.com/fakePageA",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "Test request: Missing IP address",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "hostFromPage",
				Device: &device{
					UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IPv6: "2002:0100:65fe:0:0:0:0:0",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "test.a.com",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "noRecForIP",
				Device: &device{
					UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IPv6: "2002:0100:6600:0:0:0:0:0",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "IP 2002:0100:6600:0:0:0:0:0 not found",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "deviceIPv6",
				Device: &device{
					UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "deviceIPv4",
				Device: &device{
					UA: "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IP: "1.0.101.254",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "1.0.101.254",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "country",
				Device: &device{
					UA: "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IP: "2002:0100:65fe:0:0:0:0:0",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "JP",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "region",
				Device: &device{
					UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "England",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "cityButNoRegion",
				Device: &device{
					UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
					Geo: geo{
						Country: "GB",
						Region:  "",
						City:    "notARealCity",
					},
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "London",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "city",
				Device: &device{
					UA: "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IP: "2002:0100:65fe:0:0:0:0:0",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "",
	},
	{
		data: data_BidReqValidate{
			bidReq: &bidReq{
				ID: "userHash",
				Device: &device{
					UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
				},
				Site: &site{
					Page: "http://test.a.com/fakepagea",
				},
			},
			ip:      &test_ip,
			user:    &test_user,
			country: &test_country,
			region:  &test_region,
			city:    &test_city,
			pubName: "Test",
		},
		expected: "85dc78fcd1eaef96b080ac4ca8820c23",
	},
}

func Test_BidReqValidate(t *testing.T) {
	tb := table_BidReqValidate
	testIPRec := []ip2locRec{
		{
			ToIP:        []byte{255, 255, 0, 255, 255, 255},
			CountryCode: "-",
			Region:      "-",
			City:        "-",
		},
		{
			ToIP:        []byte{255, 255, 1, 0, 101, 255},
			CountryCode: "JP",
		},
		{
			ToIP:        []byte{32, 1, 6, 124, 41, 228, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255},
			CountryCode: "GB",
			Region:      "England",
			City:        "London",
		},
	}
	ipRecs.Recs = testIPRec

	for i, j := 0, len(tb); i < j; i++ {
		res := tb[i].data.bidReq.BidReqValidate(tb[i].data.ip, tb[i].data.user, tb[i].data.country, tb[i].data.region, tb[i].data.city, tb[i].data.pubName)

		switch r := res.(type) {
		case nil:
			act := ""
			switch tb[i].data.bidReq.ID {
			case "hostFromPage":
				act = tb[i].data.bidReq.Site.Domain
			case "deviceIPv6", "deviceIPv4":
				act = *tb[i].data.ip
			case "country":
				act = *tb[i].data.country
			case "region":
				act = *tb[i].data.region
			case "city", "cityButNoRegion":
				act = *tb[i].data.city
			case "userHash":
				act = *tb[i].data.user
			}
			if act != tb[i].expected {
				t.Fatalf("Expected: %s --- Actual: %s\nTest ID: %s", tb[i].expected, act, tb[i].data.bidReq.ID)
			}
		case error:
			if err := r.Error(); err != tb[i].expected {
				t.Fatalf("Expected: %s --- Actual: %s\nTest ID: %s", tb[i].expected, err, tb[i].data.bidReq.ID)
			}
		}
	}
}

//***End of tests for BidReqValidate method on *bidReq

//***Beginning of tests for Validate method on *reqRTB

type data_Validate_reqRTB struct {
	reqRTB *reqRTB
	bidReq *bidReq
}

type test_Validate_reqRTB struct {
	data     data_Validate_reqRTB
	expected *bidReq
}

var table_Validate_reqRTB = []test_Validate_reqRTB{
	{
		data: data_Validate_reqRTB{
			reqRTB: &reqRTB{},
			bidReq: &bidReq{},
		},
		expected: &bidReq{
			ID: "reqRTB_1",
			Imp: []imp{
				{
					ID:       "1",
					BidFloor: 10.00,
					Plcmtcnt: 1,
					Assets: []impAsset{
						{
							ID:       &test_one,
							Required: 1,
							Title: &impTitle{
								Len: 25,
							},
						},
					},
				},
			},
			Site: &site{
				ID:     "siteID_1",
				Domain: "",
				Page:   "http://www.a.com/fakepage",
			},
			Device: &device{
				UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
				IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
				Geo: geo{
					Country: "GB",
					Region:  "England",
					City:    "London",
				},
				DeviceType: 3,
			},
			Bcat: []string{},
			Badv: []string{"z.com"},
		},
	},
	{
		data: data_Validate_reqRTB{
			reqRTB: &reqRTB{},
			bidReq: &bidReq{},
		},
		expected: &bidReq{
			ID: "reqRTB_2",
			Imp: []imp{
				{
					ID:       "1",
					BidFloor: 10.10,
					Plcmtcnt: 3,
					Assets: []impAsset{
						{
							ID:       &test_one,
							Required: 1,
							Title: &impTitle{
								Len: 25,
							},
						},
						{
							ID:       &test_two,
							Required: 1,
							Data: &impData{
								Type: 2,
								Len:  30,
							},
						},
						{
							ID:       &test_three,
							Required: 0,
							Data: &impData{
								Type: 10,
								Len:  50,
							},
						},
						{
							ID:       &test_four,
							Required: 1,
							Img:      &struct{}{},
						},
					},
				},
				{
					ID:       "2",
					BidFloor: 10.051,
					Plcmtcnt: 1,
					Assets: []impAsset{
						{
							ID:       &test_one,
							Required: 1,
							Title: &impTitle{
								Len: 30,
							},
						},
					},
				},
			},
			Site: &site{
				ID:     "siteID_2",
				Domain: "",
				Page:   "http://www.b.com/fakepage",
			},
			Device: &device{
				UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
				IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
				Geo: geo{
					Country: "GB",
					Region:  "England",
					City:    "London",
				},
				DeviceType: 7,
			},
			Bcat: []string{"IAB2"},
			Badv: []string{"z.com"},
		},
	},
}

var bidReq_Validate_reqRTB = []bidReq{
	{
		ID: "reqRTB_1",
		Site: &site{
			ID:     "siteID_1",
			Domain: "",
			Page:   "http://www.a.com/fakepage",
		},
		Device: &device{
			UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
			IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
			Geo: geo{
				Country: "GB",
				Region:  "England",
				City:    "London",
			},
			DeviceType: 3,
		},
		Bcat: []string{},
		Badv: []string{"z.com"},
	},
	{
		ID: "reqRTB_2",
		Site: &site{
			ID:     "siteID_2",
			Domain: "",
			Page:   "http://www.b.com/fakepage",
		},
		Device: &device{
			UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
			IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
			Geo: geo{
				Country: "GB",
				Region:  "England",
				City:    "London",
			},
			DeviceType: 7,
		},
		Bcat: []string{"IAB2"},
		Badv: []string{"z.com"},
	},
}

var rtbImp_Validate_reqRTB = [][]rtbImp{
	[]rtbImp{
		{
			ID:       "1",
			Native:   native{},
			BidFloor: 10.00,
		},
	},
	[]rtbImp{
		{
			ID:       "1",
			Native:   native{},
			BidFloor: 10.10,
		},
		{
			ID:       "2",
			Native:   native{},
			BidFloor: 10.051,
		},
	},
}

var nativeReq_Validate_reqRTB = [][]nativeReq{
	[]nativeReq{
		{
			Plcmtcnt: 1,
			Assets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
					Title: &impTitle{
						Len: 25,
					},
				},
			},
		},
	},
	[]nativeReq{
		{
			Plcmtcnt: 3,
			Assets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
					Title: &impTitle{
						Len: 25,
					},
				},
				{
					ID:       &test_two,
					Required: 1,
					Data: &impData{
						Type: 2,
						Len:  30,
					},
				},
				{
					ID:       &test_three,
					Required: 0,
					Data: &impData{
						Type: 10,
						Len:  50,
					},
				},
				{
					ID:       &test_four,
					Required: 1,
					Img:      &struct{}{},
				},
			},
		},
		{
			Plcmtcnt: 1,
			Assets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
					Title: &impTitle{
						Len: 30,
					},
				},
			},
		},
	},
}

// func (r *reqRTB) BidReqValidate(ip, user, country, region, city *string, pubName string) error {
// 	return nil
// }

// func Test_Validate_reqRTB(t *testing.T) {
// 	tv := table_Validate_reqRTB

// 	for i, j := 0, len(tv); i < j; i++ {
// 		tv[i].data.reqRTB.bidReq = bidReq_Validate_reqRTB[i]
// 		tv[i].data.reqRTB.Imp = rtbImp_Validate_reqRTB[i]

// 		for a, b := 0, len(tv[i].data.reqRTB.Imp); a < b; a++ {
// 			nativeBytes, err := json.Marshal(nativeReq_Validate_reqRTB[i][a])
// 			if err != nil {
// 				t.Fatalf("Marshal error")
// 			}
// 			nativeStr := string(nativeBytes)
// 			tv[i].data.reqRTB.Imp[a].Native.Request = nativeStr
// 		}

// 		res := tv[i].data.reqRTB.Validate(tv[i].data.bidReq, &test_ip, &test_user, &test_country, &test_region, &test_city, "Test")
// 		if res != nil {
// 			t.Fatalf("Error: %s", res.Error())
// 		}

// 		if !reflect.DeepEqual(tv[i].expected, tv[i].data.bidReq) {
// 			t.Fatalf("Expected: %v\n --- Actual: %v\nTest ID: %s", tv[i].expected, tv[i].data.bidReq, tv[i].data.reqRTB.ID)
// 		}
// 	}
// }

//***End of tests for Validate method on *reqRTB

//***Beginning of tests for Validate method on *reqAdblade

type data_Validate_reqAdblade struct {
	reqAdblade *reqAdblade
	bidReq     *bidReq
}

type test_Validate_reqAdblade struct {
	data     data_Validate_reqAdblade
	expected *bidReq
}

var impAdblade_Validate_reqAdblade = [][]impAdblade{
	[]impAdblade{
		{
			ID: "1",
			NewsBullet: newsBullet{
				Adslots: 3,
			},
		},
	},
	[]impAdblade{
		{
			ID: "1",
			NewsBullet: newsBullet{
				Adslots: 2,
			},
		},
		{
			ID: "2",
			NewsBullet: newsBullet{
				Adslots: 1,
			},
		},
	},
}

var bidReq_Validate_reqAdblade = []bidReq{
	{
		ID: "reqAdblade_1",
		Site: &site{
			ID:     "siteID_1",
			Domain: "f.com",
			Page:   "",
		},
		Device: &device{
			UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
			IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
			Geo: geo{
				Country: "GB",
				Region:  "England",
				City:    "London",
			},
			DeviceType: 3,
		},
		Bcat: []string{"IAB4"},
		Badv: []string{"z.com"},
	},
	{
		ID: "reqAdblade_2",
		Site: &site{
			ID:     "siteID_2",
			Domain: "",
			Page:   "http://www.b.com/fakepage",
		},
		Device: &device{
			UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
			IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
			Geo: geo{
				Country: "GB",
				Region:  "England",
				City:    "London",
			},
			DeviceType: 7,
		},
		Bcat: []string{"IAB2"},
		Badv: []string{"z.com"},
	},
}

var table_Validate_reqAdblade = []test_Validate_reqAdblade{
	{
		data: data_Validate_reqAdblade{
			reqAdblade: &reqAdblade{},
			bidReq:     &bidReq{},
		},
		expected: &bidReq{
			ID: "reqAdblade_1",
			Imp: []imp{
				{
					ID:       "1",
					Plcmtcnt: 3,
					Assets: []impAsset{
						{
							ID:       &test_one,
							Required: 0,
							Title: &impTitle{
								Len: 25,
							},
						},
						{
							ID:       &test_two,
							Required: 0,
							Img:      &struct{}{},
						},
						{
							ID:       &test_three,
							Required: 0,
							Data: &impData{
								Type: 1,
								Len:  25,
							},
						},
						{
							ID:       &test_four,
							Required: 0,
							Data: &impData{
								Type: 2,
								Len:  100,
							},
						},
					},
				},
			},
			Site: &site{
				ID:     "siteID_1",
				Domain: "f.com",
				Page:   "",
			},
			Device: &device{
				UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
				IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
				Geo: geo{
					Country: "GB",
					Region:  "England",
					City:    "London",
				},
				DeviceType: 3,
			},
			Bcat: []string{"IAB4"},
			Badv: []string{"z.com"},
		},
	},
	{
		data: data_Validate_reqAdblade{
			reqAdblade: &reqAdblade{},
			bidReq:     &bidReq{},
		},
		expected: &bidReq{
			ID: "reqAdblade_2",
			Imp: []imp{
				{
					ID:       "1",
					Plcmtcnt: 2,
					Assets: []impAsset{
						{
							ID:       &test_one,
							Required: 0,
							Title: &impTitle{
								Len: 25,
							},
						},
						{
							ID:       &test_two,
							Required: 0,
							Img:      &struct{}{},
						},
						{
							ID:       &test_three,
							Required: 0,
							Data: &impData{
								Type: 1,
								Len:  25,
							},
						},
						{
							ID:       &test_four,
							Required: 0,
							Data: &impData{
								Type: 2,
								Len:  100,
							},
						},
					},
				},
				{
					ID:       "2",
					Plcmtcnt: 1,
					Assets: []impAsset{
						{
							ID:       &test_one,
							Required: 0,
							Title: &impTitle{
								Len: 25,
							},
						},
						{
							ID:       &test_two,
							Required: 0,
							Img:      &struct{}{},
						},
						{
							ID:       &test_three,
							Required: 0,
							Data: &impData{
								Type: 1,
								Len:  25,
							},
						},
						{
							ID:       &test_four,
							Required: 0,
							Data: &impData{
								Type: 2,
								Len:  100,
							},
						},
					},
				},
			},
			Site: &site{
				ID:     "siteID_2",
				Domain: "",
				Page:   "http://www.b.com/fakepage",
			},
			Device: &device{
				UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
				IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
				Geo: geo{
					Country: "GB",
					Region:  "England",
					City:    "London",
				},
				DeviceType: 7,
			},
			Bcat: []string{"IAB2"},
			Badv: []string{"z.com"},
		},
	},
}

// func (r *reqAdblade) BidReqValidate(ip, user, country, region, city *string, pubName string) error {
// 	return nil
// }

// func Test_Validate_reqAdblade(t *testing.T) {
// 	tv := table_Validate_reqAdblade

// 	for i, j := 0, len(tv); i < j; i++ {
// 		tv[i].data.reqAdblade.bidReq = bidReq_Validate_reqAdblade[i]
// 		tv[i].data.reqAdblade.Imp = impAdblade_Validate_reqAdblade[i]

// 		res := tv[i].data.reqAdblade.Validate(tv[i].data.bidReq, &test_ip, &test_user, &test_country, &test_region, &test_city, "Test")
// 		if res != nil {
// 			t.Fatalf("Error: %s", res.Error())
// 		}

// 		if !reflect.DeepEqual(tv[i].expected, tv[i].data.bidReq) {
// 			t.Fatalf("Expected: %v\n --- Actual: %v\nTest ID: %s", tv[i].expected, tv[i].data.bidReq, tv[i].data.reqAdblade.ID)
// 		}
// 	}
// }

//***End of tests for Validate method on *reqAdblade

//***Beginning of tests for StandardizeBidReq

type data_StandardizeBidReq struct {
	brs                    *bidReqStandardizer
	pubID, seatID, pubName string
	pubRev                 float64
	body                   bodyReadCloser
}

type test_StandardizeBidReq struct {
	data     data_StandardizeBidReq
	expected expected_StandardizeBidReq
}

type expected_StandardizeBidReq struct {
	bidReq                          bidReq
	ip, user, country, region, city string
	pubRev                          float64
	err                             error
}

type bodyReadCloser struct {
	io.Reader
}

func (bodyReadCloser) Close() error {
	return nil
}

var table_StandardizeBidReq = []test_StandardizeBidReq{
	{
		data: data_StandardizeBidReq{
			brs:     &bidReqStandardizer{},
			pubID:   "1",
			seatID:  "1",
			pubName: "Adblade",
			pubRev:  .7,
			body: bodyReadCloser{
				(func() io.Reader {
					tv := table_Validate_reqAdblade
					tv[0].data.reqAdblade.bidReq = bidReq_Validate_reqAdblade[0]
					tv[0].data.reqAdblade.Imp = impAdblade_Validate_reqAdblade[0]
					res, err := json.Marshal(tv[0].data.reqAdblade)
					if err != nil {
						return bytes.NewBufferString("Error creating bodyReadCloser in seatID 1")
					}
					return bytes.NewBuffer(res)
				})(),
			},
		},
		expected: expected_StandardizeBidReq{
			bidReq: bidReq{
				ID: "reqAdblade_1",
				Imp: []imp{
					{
						ID:       "1",
						Plcmtcnt: 3,
						Assets: []impAsset{
							{
								ID:       &test_one,
								Required: 0,
								Title: &impTitle{
									Len: 25,
								},
							},
							{
								ID:       &test_two,
								Required: 0,
								Img:      &struct{}{},
							},
							{
								ID:       &test_three,
								Required: 0,
								Data: &impData{
									Type: 1,
									Len:  25,
								},
							},
							{
								ID:       &test_four,
								Required: 0,
								Data: &impData{
									Type: 2,
									Len:  100,
								},
							},
						},
					},
				},
				Site: &site{
					ID:     "siteID_1",
					Domain: "f.com",
					Page:   "",
				},
				Device: &device{
					UA:   "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25",
					IPv6: "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
					Geo: geo{
						Country: "GB",
						Region:  "England",
						City:    "London",
					},
					DeviceType: 3,
				},
				Bcat: []string{"IAB4"},
				Badv: []string{"z.com"},
			},
			ip:      "2001:67c:29e4:ffff:ffff:ffff:ffff:ffff",
			user:    "85dc78fcd1eaef96b080ac4ca8820c23",
			country: "GB",
			region:  "England",
			city:    "London",
			pubRev:  .7,
			err:     nil,
		},
	},
}

//NOTE: Test functions for Validate methods in seat module must be commented
//out for this test. This could be remedied by passing in real values for
//BidReqValidate's params, but this would defeat the purpose of putting these
//methods in interfaces, allowing them to be tested while mocking the return
//values of methods called within other methods.

func Test_StandardizeBidReq(t *testing.T) {
	ts := table_StandardizeBidReq

	for i, j := 0, len(ts); i < j; i++ {
		publishersMutex.Lock()
		seatsMutex.Lock()
		if _, exists := publishers[ts[i].data.pubID]; ts[i].data.pubID != "" && !exists {
			publishers[ts[i].data.pubID] = pub{
				Name:     ts[i].data.pubName,
				Revshare: ts[i].data.pubRev,
			}
		}
		if _, exists := seats[ts[i].data.seatID]; ts[i].data.seatID != "" && !exists {
			seats[ts[i].data.seatID] = ts[i].data.pubID
		}
		publishersMutex.Unlock()
		seatsMutex.Unlock()

		br, ip, user, country, region, city, pubRev, err := ts[i].data.brs.StandardizeBidReq(bv, ts[i].data.seatID, ts[i].data.body)

		switch {
		case !reflect.DeepEqual(br, ts[i].expected.bidReq):
			t.Fatalf("Expected: %v\n --- Actual: %v\nStandardizeBidReq: %s", ts[i].expected.bidReq, br, "bidReq")
		case ip != ts[i].expected.ip:
			t.Fatalf("Expected: %s\n --- Actual: %s\nStandardizeBidReq: %s", ts[i].expected.ip, ip, "ip")
		case user != ts[i].expected.user:
			t.Fatalf("Expected: %s\n --- Actual: %s\nStandardizeBidReq: %s", ts[i].expected.user, user, "user")
		case country != ts[i].expected.country:
			t.Fatalf("Expected: %s\n --- Actual: %s\nStandardizeBidReq: %s", ts[i].expected.country, country, "country")
		case region != ts[i].expected.region:
			t.Fatalf("Expected: %s\n --- Actual: %s\nStandardizeBidReq: %s", ts[i].expected.region, region, "region")
		case city != ts[i].expected.city:
			t.Fatalf("Expected: %s\n --- Actual: %s\nStandardizeBidReq: %s", ts[i].expected.city, city, "city")
		case pubRev != ts[i].expected.pubRev:
			t.Fatalf("Expected: %d\n --- Actual: %d\nStandardizeBidReq: %s", ts[i].expected.pubRev, pubRev, "pubRev")
		case err != ts[i].expected.err:
			t.Fatalf("Expected: %v\n --- Actual: %v\nStandardizeBidReq: %s", ts[i].expected.err, err, "err")
		}
	}
}

//***End of tests for StandardizeBidReq

//***End of seat.go testing

//***Beginning of tests for adsGO.go

//***Beginning of tests for ValidateAssets

type test_ValidateAssets struct {
	imp      imp
	expected expected_ValidateAssets
}

type expected_ValidateAssets struct {
	nextImp, desc2 bool
	descLen        int
}

var table_ValidateAssets = []test_ValidateAssets{
	{
		imp: imp{},
		expected: expected_ValidateAssets{
			nextImp: true,
			desc2:   false,
			descLen: -1,
		},
	},
	{
		imp: imp{
			ID: "1",
			Assets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
					Data: &impData{
						Type: 6,
						Len:  25,
					},
				},
			},
		},
		expected: expected_ValidateAssets{
			nextImp: true,
			desc2:   false,
			descLen: -1,
		},
	},
	{
		imp: imp{
			ID: "2",
			Assets: []impAsset{
				{
					ID:       &test_two,
					Required: 1,
					Data: &impData{
						Type: 2,
						Len:  40,
					},
				},
			},
		},
		expected: expected_ValidateAssets{
			nextImp: false,
			desc2:   false,
			descLen: 40,
		},
	},
	{
		imp: imp{
			ID: "3",
			Assets: []impAsset{
				{
					ID:       &test_three,
					Required: 1,
					Data: &impData{
						Type: 10,
						Len:  3000,
					},
				},
			},
		},
		expected: expected_ValidateAssets{
			nextImp: false,
			desc2:   true,
			descLen: -1,
		},
	},
}

func Test_ValidateAssets(t *testing.T) {
	av := &assetValidator{}

	for i, j := 0, len(table_ValidateAssets); i < j; i++ {
		nextImp, desc2, descLen := av.ValidateAssets(table_ValidateAssets[i].imp)
		switch {
		case nextImp != table_ValidateAssets[i].expected.nextImp:
			t.Fatalf("Expected: %s\n --- Actual: %s\nValidateAssets: %s", table_ValidateAssets[i].expected.nextImp, nextImp, "nextImp")
		case desc2 != table_ValidateAssets[i].expected.desc2:
			t.Fatalf("Expected: %s\n --- Actual: %s\nValidateAssets: %s", table_ValidateAssets[i].expected.desc2, desc2, "desc2")
		case descLen != table_ValidateAssets[i].expected.descLen:
			t.Fatalf("Expected: %d\n --- Actual: %d\nValidateAssets: %s", table_ValidateAssets[i].expected.descLen, descLen, "descLen")
		}
	}
}

//***End of tests for ValidateAssets

//***Beginning of tests for FilterAssets

type test_FilterAssets struct {
	data     data_FilterAssets
	expected expected_FilterAssets
}

type data_FilterAssets struct {
	cr        creative
	impAssets []impAsset
	desc2     bool
	descLen   int
}

type expected_FilterAssets struct {
	crAssets []crAsset
	err      error
}

var table_FilterAssets = []test_FilterAssets{
	{
		data: data_FilterAssets{
			impAssets: []impAsset{
				{
					Required: 1,
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{},
			err:      errors.New("Required impression asset missing ID"),
		},
	},
	{
		data: data_FilterAssets{
			impAssets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{},
			err:      errors.New("Creative cannot fulfill a required asset of impression"),
		},
	},
	{
		data: data_FilterAssets{
			impAssets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
					Data: &impData{
						Type: 6,
					},
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{},
			err:      errors.New("Creative cannot fulfill a required asset of impression"),
		},
	},
	{
		data: data_FilterAssets{
			cr: creative{
				ID:    1,
				Title: "This is a title for creative 1.",
				Desc:  "This is a description for creative 2. It is missing an ID, but is not required, so creative 2 is still eligible.",
			},
			impAssets: []impAsset{
				{
					Required: 0,
					Data: &impData{
						Type: 2,
						Len:  25,
					},
				},
				{
					ID:       &test_three,
					Required: 1,
					Title: &impTitle{
						Len: 0,
					},
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{
				{
					ID: 3,
					Title: &crTitle{
						Text: "",
					},
				},
			},
		},
	},
	{
		data: data_FilterAssets{
			cr: creative{
				ID:    1,
				Title: "This is a title for creative 1.",
			},
			impAssets: []impAsset{
				{
					ID:       &test_two,
					Required: 1,
					Title: &impTitle{
						Len: 25,
					},
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{
				{
					ID: 2,
					Title: &crTitle{
						Text: "This is a title for...",
					},
				},
			},
		},
	},
	{
		data: data_FilterAssets{
			cr: creative{
				ID:    1,
				Title: "blahblahblah.",
			},
			impAssets: []impAsset{
				{
					ID:       &test_two,
					Required: 1,
					Title: &impTitle{
						Len: 3,
					},
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{
				{
					ID: 2,
					Title: &crTitle{
						Text: "...",
					},
				},
			},
		},
	},
	{
		data: data_FilterAssets{
			cr: creative{
				ID:    1,
				Title: "Desc not required, so this passes.",
			},
			impAssets: []impAsset{
				{
					ID:       &test_two,
					Required: 0,
					Title: &impTitle{
						Len: 31, //One less than title len after trim
					},
				},
				{
					ID:       &test_three,
					Required: 0,
					Data: &impData{
						Type: 2,
						Len:  25,
					},
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{
				{
					ID: 2,
					Title: &crTitle{
						Text: "Desc not required, so this...",
					},
				},
			},
		},
	},
	{
		data: data_FilterAssets{
			cr: creative{
				ID:   1,
				Desc: "This is the main description.",
			},
			impAssets: []impAsset{
				{
					ID:       &test_two,
					Required: 1,
					Data: &impData{
						Type: 10,
						Len:  25,
					},
				},
				{
					ID:       &test_three,
					Required: 1,
					Data: &impData{
						Type: 2,
						Len:  25,
					},
				},
			},
			desc2:   true,
			descLen: 25,
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{
				{
					ID: 2,
					Data: &crData{
						Value: "the main...",
					},
				},
				{
					ID: 3,
					Data: &crData{
						Value: "This is ",
					},
				},
			},
		},
	},
	{
		data: data_FilterAssets{
			cr: creative{
				ID:      1,
				Display: "Coca-Cola",
			},
			impAssets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
					Data: &impData{
						Type: 11,
						Len:  25,
					},
				},
				{
					ID:       &test_two,
					Required: 1,
					Data: &impData{
						Type: 12,
						Len:  25,
					},
				},
			},
		},
		expected: expected_FilterAssets{
			crAssets: []crAsset{
				{
					ID: 1,
					Data: &crData{
						Value: "Coca-Cola",
					},
				},
				{
					ID: 2,
					Data: &crData{
						Value: "Learn More",
					},
				},
			},
		},
	},
}

func Test_FilterAssets(t *testing.T) {
	af := &assetFilter{}
	tf := table_FilterAssets

	for i, j := 0, len(tf); i < j; i++ {
		crAssets, err := af.FilterAssets(tf[i].data.cr, tf[i].data.impAssets, tf[i].data.desc2, tf[i].data.descLen)
		if err != nil && err.Error() != tf[i].expected.err.Error() {
			t.Fatalf("Error in FilterAssets: %s", err.Error())
		}
		if !reflect.DeepEqual(crAssets, tf[i].expected.crAssets) {
			exp, err := json.Marshal(tf[i].expected.crAssets)
			if err != nil {
				t.Fatalf("Marshal err: exp")
			}
			act, err := json.Marshal(crAssets)
			if err != nil {
				t.Fatalf("Marshal err: act")
			}
			t.Fatalf("Expected: %s\n --- Actual: %s\ntable_FilterAssets index: %d", string(exp), string(act), i)
		}
	}
}

//***End of tests for FilterAssets

//***Beginning of tests for FilterCreatives

type test_FilterCreatives struct {
	data     data_FilterCreatives
	expected expected_FilterCreatives
}

type data_FilterCreatives struct {
	descLen   int
	desc2     bool
	crids     map[int]struct{}
	impAssets []impAsset
}

type expected_FilterCreatives struct {
	crid     int
	dest     string
	crAssets []crAsset
}

var creatives_Test_FilterCreatives = []creative{
	{
		ID:      1,
		Title:   "This is title for creative 1",
		Display: "Coca-Cola",
		Dest:    "http://coca-cola.com",
		Img:     "https://api.creatives.coke.com",
	},
	{
		ID:      2,
		Title:   "This is title for creative 2",
		Display: "Coca-Cola",
		Dest:    "http://coca-cola.com",
		Img:     "https://api.creatives.coke.com",
	},
	{
		ID:      3,
		Title:   "This is title for creative 3",
		Display: "Coca-Cola",
		Dest:    "http://coca-cola.com",
		Img:     "https://api.creatives.coke.com",
		Desc:    "Try our new holiday flavor, 12 days of cavities!",
	},
}

var table_FilterCreatives = []test_FilterCreatives{
	{
		data: data_FilterCreatives{
			descLen: 50,
			desc2:   false,
			crids: map[int]struct{}{
				1: struct{}{},
				2: struct{}{},
				3: struct{}{},
			},
			impAssets: []impAsset{
				{
					ID:       &test_one,
					Required: 1,
					Title: &impTitle{
						Len: 30,
					},
				},
				{
					ID:       &test_two,
					Required: 1,
					Data: &impData{
						Type: 1,
						Len:  30,
					},
				},
				{
					ID:       &test_three,
					Required: 1,
					Data: &impData{
						Type: 2,
						Len:  50,
					},
				},
				{
					ID:       &test_four,
					Required: 1,
					Img:      &struct{}{},
				},
				{
					ID:       &test_five,
					Required: 1,
					Data: &impData{
						Type: 11,
						Len:  30,
					},
				},
			},
		},
		expected: expected_FilterCreatives{
			crid: 3,
			dest: "http://coca-cola.com",
			crAssets: []crAsset{
				{
					ID: 1,
					Title: &crTitle{
						Text: "This is title for creative 3",
					},
				},
				{
					ID: 2,
					Data: &crData{
						Value: "Coca-Cola",
					},
				},
				{
					ID: 3,
					Data: &crData{
						Value: "Try our new holiday flavor, 12 days of cavities!",
					},
				},
				{
					ID: 4,
					Img: &crImg{
						URL: "https://api.creatives.coke.com",
					},
				},
				{
					ID: 5,
					Data: &crData{
						Value: "Coca-Cola",
					},
				},
			},
		},
	},
}

//Define functions

func Test_FilterCreatives(t *testing.T) {
	crf := &creativeFilter{}
	tf := table_FilterCreatives
	oldAsNewKey := asNewKey
	oldClientGet := clientGet
	oldClientBatchGet := clientBatchGet

	defer func() {
		asNewKey = oldAsNewKey
		clientGet = oldClientGet
		clientBatchGet = oldClientBatchGet
	}()

	//For the sake of testing, id and recID are the same
	asNewKey = func(namespace, set string, id int) (recID int, err error) {

	}

	for i, j := 0, len(tf); i < j; i++ {
		for id := range tf[i].data.crids {
			creatives[id] = creatives_Test_FilterCreatives[id-1]
		}
		crAssets, dest, crid := crf.FilterCreatives(af, tf[i].data.descLen, tf[i].data.desc2, tf[i].data.crids, tf[i].data.impAssets)
		switch {
		case dest != tf[i].expected.dest:
			t.Fatalf("Expected: %s\n --- Actual: %s\ntable_FilterCreatives index: %d", tf[i].expected.dest, dest, i)
		case crid != tf[i].expected.crid:
			t.Fatalf("Expected: %s\n --- Actual: %s\ntable_FilterCreatives index: %d", tf[i].expected.crid, crid, i)
		case !reflect.DeepEqual(crAssets, tf[i].expected.crAssets):
			exp, err := json.Marshal(tf[i].expected.crAssets)
			if err != nil {
				t.Fatalf("Marshal err: exp")
			}
			act, err := json.Marshal(crAssets)
			if err != nil {
				t.Fatalf("Marshal err: act")
			}
			t.Fatalf("Expected: %s\n --- Actual: %s\ntable_FilterCreatives index: %d", string(exp), string(act), i)
		}
	}
}

//***End of tests for FilterCreatives

//***End of tests for adsGO.go

// import (
// 	"bytes"
// 	// "encoding/json"
// 	// "fmt"
// 	"github.com/StevenRispoli/adsGO/fields"
// 	. "github.com/StevenRispoli/throwFatalErr" //For testing only
// 	// "github.com/oschwald/maxminddb-golang"
// 	"github.com/ua-parser/uap-go/uaparser"
// 	// "io/ioutil"
// 	// "log"
// 	"net/http"
// 	"net/http/httptest"
// 	"testing"
// 	"time"
// )

// type test struct {
// 	URL  string
// 	Data []data
// }

// type data struct {
// 	Body     []byte
// 	Expected int
// }

// var testData = []test{
// 	{
// 		"45.55.157.222:3001/bid?seat_id=1",
// 		[]data{
// 			data{
// 				[]byte(
// 					`{
// 						"id": "adblade",
// 						"imp": [{"id": "1", "newsbullet": {"adslots": 1}}],
// 						"site": {"id": "adbladeSite", "domain": "adbladeDomain.com"},
// 						"device": {"ua": "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25", "ip": "2002:0100:65fe:0:0:0:0:0"},
// 						"user": {"id": "someFakeUserID"},
// 						"bcat": ["IAB2"],
// 						"badv": ["a.com", "b.com", "d.com"]
// 					}`,
// 				),
// 				200,
// 			},
// 		},
// 	},
// 	{
// 		"45.55.157.222:3001/bid?seat_id=2",
// 		[]data{
// 			data{
// 				[]byte(
// 					`{
// 						"id": "genericPub",
// 						"imp": [{"id": "1", "native": {"request": "{\"plcmtcnt\": 1, \"assets\": [{\"id\": 1, \"title\": {\"len\": 25}}, {\"id\": 2, \"data\": {\"type\": 1, \"len\": 25}}]}"}}],
// 						"site": {"id": "genericSite", "domain": "genericDomain.com"},
// 						"device": {"ua": "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25", "ip": "2600:380:585d:a089:4d53:139b:e1ec:e17"},
// 						"user": {"id": "anotherFakeUserID"},
// 						"bcat": ["IAB2"],
// 						"badv": ["a.com", "b.com", "d.com"]
// 					}`,
// 				),
// 				200,
// 			},
// 		},
// 	},
// }

// func Test_handleBid(t *testing.T) {
// 	manualCampaigns()

// 	parser, err := uaparser.New("regexes.yaml")
// 	CheckFatalErr(err)
// 	// db, err := maxminddb.Open("GeoIP2-City.mmdb")
// 	// CheckFatalErr(err)

// 	timeout := time.Duration(180 * time.Second)
// 	client := http.Client{
// 		Timeout: timeout,
// 	}
// 	resp, err := client.Get("http://45.55.157.222:3001/updateIP2Loc")
// 	if err != nil {
// 		t.Fatalf("Update err: %v", err)
// 	}
// 	resp.Body.Close()

// 	for i := 0; i < len(testData); i++ {
// 		for j := 0; j < len(testData[i].Data); j++ {
// 			req, err := http.NewRequest("POST", testData[i].URL, bytes.NewBuffer(testData[i].Data[j].Body))
// 			req.Header.Set("Content-Type", "application/json")
// 			CheckFatalErr(err)

// 			res := httptest.NewRecorder()
// 			bidHandler(res, req, parser)
// 			exp := testData[i].Data[j].Expected
// 			act := res.Code

// 			if exp != act {
// 				t.Fatalf("Expected: %d --- Actual: %d", exp, act)
// 			}
// 		}
// 	}
// }

// func Test_reqRTBValidate(t *testing.T) {
// 	manualCampaigns()

// 	var r reqRTB
// 	var br bidReq
// 	var ip, user string
// 	var geoFromIP bool

// 	err := json.Unmarshal([]byte(
// 		`{
// 						"id": "genericPub",
// 						"imp": [{"id": "1", "native": {"request": "{\"plcmtcnt\": 3, \"assets\": [{\"id\": 1, \"title\": {\"len\": 25}}, {\"id\": 2, \"data\": {\"type\": 1, \"len\": 25}}]}"}}],
// 						"site": {"id": "genericSite", "domain": "genericDomain.com"},
// 						"device": {"ua": "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B329 Safari/8536.25", "ip": "2600:380:585d:a089:4d53:139b:e1ec:e17"},
// 						"user": {"id": "anotherFakeUserID"},
// 						"bcat": ["IAB2"],
// 						"badv": ["a.com", "b.com", "d.com"]
// 					}`,
// 	), &r)

// 	if err != nil {
// 		fmt.Print("Unmarshal error: ")
// 		fmt.Println(err)
// 	}

// 	act := r.Validate(&br, &ip, &user, &geoFromIP, "genericPub")
// 	fmt.Println(act)

// 	if act != nil {
// 		t.Fatal(act)
// 	}
// }

// func manualCampaigns() {
// 	campaigns[1] = campaign{
// 		ID:         1,
// 		AdvID:      1,
// 		CPM:        25.00,
// 		Flow:       100,
// 		Fcap:       2,
// 		HasDesc:    true,
// 		Bpub:       map[string]bool{"c.com": true},
// 		Wpub:       map[string]bool{},
// 		Cat:        map[string]bool{"IAB24": true},
// 		Geo:        map[string]map[string]map[string]bool{"JP": map[string]map[string]bool{}},
// 		DeviceType: map[int]bool{},
// 		OS:         map[string]bool{"iOS_6_1_3_iphone": true},
// 		Browser:    map[string]bool{"Mobile Safari": true},
// 		Domains:    map[string]map[int]bool{"c.com": {1: true}},
// 	}

// 	c1 := creative{
// 		ID:         1,
// 		Title:      "C_1 CR_1 Title",
// 		Display:    "C_1 CR_1 Display",
// 		Desc:       "C_1 CR_1 Description",
// 		Rating:     "",
// 		Likes:      "",
// 		Downloads:  "",
// 		Desc2:      "",
// 		DisplayURL: "c.com",
// 		C2A:        "Learn More",
// 		Dest:       "c.com",
// 		Img:        "c.com/img",
// 	}

// 	creatives[1] = c1

// 	fields.Country["JP"][1] = true
// 	fields.OS["iOS_6_1_3_iphone"][1] = true
// 	fields.Browser["Mobile Safari"][1] = true

// 	seats["1"] = "1"
// 	publishers["1"] = pub{Name: "adblade", Revshare: 0.8}

// 	seats["2"] = "2"
// 	publishers["2"] = pub{Name: "genericPub", Revshare: 0.7}

// 	return
// }
