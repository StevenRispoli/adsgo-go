package main

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"math/big"
	"net"
	"net/url"
	"sort"
	"strings"
	"sync"
	// . "github.com/StevenRispoli/throwFatalErr" //For testing only
)

var (
	//seatID mapped to pubID
	seatsMutex sync.RWMutex
	seats      = map[string]string{}

	//pubID mapped to publisher names and revshares
	publishersMutex sync.RWMutex
	publishers      = map[string]pub{}

	//seatID mapped to struct type
	nonRTB = map[string]interface{}{
		"1": reqAdblade{},
	}
)

type validator interface {
	Validate(br *bidReq, ip, user, country, region, city *string, pubName string) error
}

type pub struct {
	Name     string
	Revshare float64
}

//For further processing by bidder
type bidReq struct {
	ID     string
	Imp    []imp
	Site   *site
	Device *device
	User   user
	Bcat   []string
	Badv   []string
}

type imp struct {
	ID       string
	Assets   []impAsset
	BidFloor float64
	Plcmtcnt int
}

type impAsset struct {
	ID       *int
	Required int
	Title    *impTitle
	Data     *impData
	Img      *struct{}
	Video    *struct{}
}

type impTitle struct {
	Len int
}

type impData struct {
	Type int
	Len  int
}

type site struct {
	ID     string
	Domain string
	Page   string
}

type device struct {
	UA         string
	IP         string
	IPv6       string
	Geo        geo
	DeviceType int
	DIDSHA1    string
	DIDMD5     string
	MACSHA1    string
	MACMD5     string
}

type user struct {
	ID       string
	BuyerUID string
}

type geo struct {
	Country string
	Region  string
	City    string
}

//For exchanges that don't have their own bid request specs
type reqRTB struct {
	bidReq
	Imp []rtbImp
}

type rtbImp struct {
	ID       string
	Native   native
	BidFloor float64
}

type native struct {
	Request string
}

type nativeReq struct {
	Plcmtcnt int
	Assets   []impAsset
}

//Adblade Bid Request
type reqAdblade struct {
	bidReq
	Imp []impAdblade
}

type impAdblade struct {
	ID         string
	NewsBullet newsBullet
}

type newsBullet struct {
	Adslots int
}

type validateBidder interface {
	ValidateBid(r validator, br *bidReq, ip, user, country, region, city *string, pubName string) error
}

type bidValidator struct{}

type standardizeBidReqer interface {
	StandardizeBidReq(bv validateBidder, seatID string, body io.ReadCloser) (bidReq, string, string, string, string, string, float64, error)
}

type bidReqStandardizer struct{}

func (bv *bidValidator) ValidateBid(r validator, br *bidReq, ip, user, country, region, city *string, pubName string) error {
	return r.Validate(br, ip, user, country, region, city, pubName)
}

func (brs *bidReqStandardizer) StandardizeBidReq(bv validateBidder, seatID string, body io.ReadCloser) (bidReq, string, string, string, string, string, float64, error) {
	var br bidReq
	var ip, user, country, region, city string
	var pubRev float64
	var err error

	publishersMutex.RLock()
	seatsMutex.RLock()
	defer publishersMutex.RUnlock()
	defer seatsMutex.RUnlock()

	if pubID, exists := seats[seatID]; exists {
		var r validator
		pubRev = publishers[pubID].Revshare
		pubName := publishers[pubID].Name

		switch getPubType(pubID).(type) {
		case reqAdblade:
			r = &reqAdblade{}
		default:
			r = &reqRTB{}
		}

		err = json.NewDecoder(body).Decode(&r)
		if err != nil {
			return br, ip, user, country, region, city, pubRev, err
		}

		err = bv.ValidateBid(r, &br, &ip, &user, &country, &region, &city, pubName)

	} else {
		err = fmt.Errorf("Invalid SeatID")
	}

	return br, ip, user, country, region, city, pubRev, err
}

//creates a custom bid response based on what is required by the exchange
func bidResMaker(seatID string, br *bidRes) error {
	if seatID == "1" {
		return nil
	}
	return nil
}

func getPubType(s string) interface{} {
	if t, exists := nonRTB[s]; exists {
		return t
	}
	return reqRTB{}
}

func (r *bidReq) BidReqValidate(ip, user, country, region, city *string, pubName string) error {
	switch {
	case r.ID == "":
		return fmt.Errorf("%s request: Missing or empty ID", pubName)
	case r.Device == nil:
		return fmt.Errorf("%s request: Missing device object", pubName)
	case r.Device.UA == "":
		return fmt.Errorf("%s request: Missing UA", pubName)
	case r.Site == nil:
		return fmt.Errorf("%s request: Missing site object", pubName)
	case r.Site.Domain == "":
		if r.Site.Page == "" {
			return fmt.Errorf("%s request: Missing domain and page info", pubName)
		} else {
			if u, err := url.Parse(r.Site.Page); err != nil {
				return fmt.Errorf("%s request: Error parsing domain from page info", pubName)
			} else {
				r.Site.Domain = u.Host
			}
		}
	}

	if r.Device.IPv6 != "" {
		*ip = r.Device.IPv6
	} else if r.Device.IP != "" {
		*ip = r.Device.IP
	} else {
		return fmt.Errorf("%s request: Missing IP address", pubName)
	}

	if r.Device.Geo.Country != "" && !(r.Device.Geo.Region == "" && r.Device.Geo.City != "") {
		*country = strings.ToUpper(r.Device.Geo.Country)
		*region = r.Device.Geo.Region
		*city = r.Device.Geo.City
	} else {
		parsedIP := net.ParseIP(*ip)
		ipNum := big.NewInt(0)

		if (*ip)[:4] == "2002" {
			ipNum.SetBytes(parsedIP[2:6].To16())
		} else {
			ipNum.SetBytes(parsedIP.To16())
		}

		ipBytes := ipNum.Bytes()

		ipRecs.mu.RLock()
		defer ipRecs.mu.RUnlock()

		if len(ipRecs.Recs) < 1 {
			return fmt.Errorf("IP %s not found: No IP2Location data.", *ip)
		}

		i := sort.Search(len(ipRecs.Recs), func(i int) bool {
			//bytes.Compare is lexigraphical
			//don't compare byte arrays that are different lengths
			if len(ipRecs.Recs[i].ToIP) != len(ipBytes) {
				return false
			}
			return bytes.Compare(ipRecs.Recs[i].ToIP, ipBytes) >= 0
		})

		if i == len(ipRecs.Recs) {
			return fmt.Errorf("IP %s not found", *ip)
		}
		*country = ipRecs.Recs[i].CountryCode
		*region = ipRecs.Recs[i].Region
		*city = ipRecs.Recs[i].City
	}

	switch {
	case r.User.ID != "":
		*user = r.User.ID
	case r.User.BuyerUID != "":
		*user = r.User.BuyerUID
	case r.Device.DIDSHA1 != "":
		*user = r.Device.DIDSHA1
	case r.Device.DIDMD5 != "":
		*user = r.Device.DIDMD5
	case r.Device.MACSHA1 != "":
		*user = r.Device.MACSHA1
	case r.Device.MACMD5 != "":
		*user = r.Device.MACMD5
	default:
		hasher := md5.New()
		hasher.Write([]byte(*ip + r.Device.UA))
		*user = hex.EncodeToString(hasher.Sum(nil))
	}

	return nil
}

func (r *reqRTB) Validate(br *bidReq, ip, user, country, region, city *string, pubName string) error {
	if len(r.Imp) < 1 {
		return fmt.Errorf("%s request: Missing impressions", pubName)
	} else if err := r.BidReqValidate(ip, user, country, region, city, pubName); err != nil {
		return err
	}

	for i, j := 0, len(r.Imp); i < j; i++ {
		var nr nativeReq
		if err := json.Unmarshal([]byte(r.Imp[i].Native.Request), &nr); err != nil {
			return err
		}
		br.Imp = append(br.Imp, imp{ID: r.Imp[i].ID, BidFloor: r.Imp[i].BidFloor, Plcmtcnt: nr.Plcmtcnt, Assets: nr.Assets})
	}

	br.ID = r.ID
	br.Site = r.Site
	br.Device = r.Device
	br.Badv = r.Badv
	br.Bcat = r.Bcat
	return nil
}

func (r *reqAdblade) Validate(br *bidReq, ip, user, country, region, city *string, pubName string) error {
	if len(r.Imp) < 1 {
		return fmt.Errorf("%s request: Missing impressions", pubName)
	} else if err := r.BidReqValidate(ip, user, country, region, city, pubName); err != nil {
		return err
	}
	one, two, three, four := 1, 2, 3, 4
	assets := []impAsset{
		impAsset{ID: &one, Required: 0, Title: &impTitle{Len: 25}},
		impAsset{ID: &two, Required: 0, Img: &struct{}{}},
		impAsset{ID: &three, Required: 0, Data: &impData{Type: 1, Len: 25}},
		impAsset{ID: &four, Required: 0, Data: &impData{Type: 2, Len: 100}},
	}

	for i, j := 0, len(r.Imp); i < j; i++ {
		br.Imp = append(br.Imp, imp{ID: r.Imp[i].ID, Assets: assets, Plcmtcnt: r.Imp[i].NewsBullet.Adslots})
	}

	br.ID = r.ID
	br.Site = r.Site
	br.Device = r.Device
	br.Badv = r.Badv
	br.Bcat = r.Bcat
	return nil
}
