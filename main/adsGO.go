package main

import (
	// "io"
	// "os"
	"fmt"
	"log"
	// "net"
	// "math"
	// "io/ioutil"
	"math/big"
	"math/rand"
	"net/http"
	"net/url"
	// "runtime"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
	// "crypto/md5"
	// "encoding/hex"
	"encoding/json"
	"github.com/StevenRispoli/adsGO/fields"
	"github.com/StevenRispoli/genid"
	"github.com/StevenRispoli/stringy"
	. "github.com/StevenRispoli/throwFatalErr" //For testing only
	// "github.com/oschwald/maxminddb-golang"
	as "github.com/aerospike/aerospike-client-go"
	"github.com/ua-parser/uap-go/uaparser"
)

const (
	serverPort = ":3001"
)

//TO DO: Backup data with Aerospike

var (
	asNewKey       = as.NewKey
	client         = &as.Client{}
	clientGet      = client.Get
	clientBatchGet = client.BatchGet

	uaMutex sync.Mutex
	uaCache = map[string]parsedUA{}

	//Mutex to guard Country, Browser, and OS in fields package
	fieldsMutex sync.RWMutex

	campaignsMutex sync.RWMutex
	campaigns      = map[int]campaign{}

	creativesMutex sync.RWMutex
	creatives      = map[int]creative{}

	//Has its own RWMutex
	ipRecs ipRecords

	brs = &bidReqStandardizer{}
	caf = &campaignFilter{}
	bv  = &bidValidator{}
	crf = &creativeFilter{}
	av  = &assetValidator{}
	af  = &assetFilter{}

	methodErr = fmt.Errorf("Forbidden HTTP method")

	distros = map[string]struct{}{
		"CentOS":    struct{}{},
		"Debian":    struct{}{},
		"Fedora":    struct{}{},
		"Gentoo":    struct{}{},
		"Kubuntu":   struct{}{},
		"Mageia":    struct{}{},
		"Mandriva":  struct{}{},
		"Red Hat":   struct{}{},
		"Slackware": struct{}{},
		"SUSE":      struct{}{},
		"Ubuntu":    struct{}{},
		"openSUSE":  struct{}{},
	}
)

//Advertisers only need to add deviceTypes 3, 6, and 7
type campaign struct {
	ID         int
	AdvID      int
	CPM        float64
	Flow       int
	Fcap       int
	HasDesc    bool
	Bpub       map[string]bool
	Wpub       map[string]bool
	Cat        map[string]bool
	Geo        map[string]map[string]map[string]bool
	DeviceType map[int]bool
	OS         map[string]bool
	Browser    map[string]bool
	Domains    map[string][]int
}

//Title, Display, Destination, and Img must be added
//by advertiser before creative can be added to database
type creative struct {
	ID         int
	Title      string //Bid request Title asset
	Display    string //Bid request Data asset type 1
	Desc       string //Bid request Data asset type 2
	Rating     string //Bid request Data asset type 3
	Likes      string //Bid request Data asset type 4
	Downloads  string //Bid request Data asset type 5
	Desc2      string //Bid request Data asset type 10
	DisplayURL string //Bid request Data asset type 11
	C2A        string //Bid request Data asset type 12
	Dest       string //URL for link obj in native res obj markup
	Img        string //URL for Bid response img asset
}

type bidRes struct {
	ID      string    `json:"id"`
	SeatBid []seatBid `json:"seatbid"`
}

type seatBid struct {
	Bid  []bid  `json:"bid"`
	Seat string `json:"seat,omitempty"`
}

type bid struct {
	ID    string  `json:"id"`
	ImpID string  `json:"impid"`
	Price float64 `json:"price"`
	NURL  string  `json:"nurl"`
	ADM   string  `json:"adm"`
}

type adm struct {
	Assets []crAsset `json:"assets"`
	Link   link      `json:"link"`
}

type crAsset struct {
	ID    int      `json:"id"`
	Title *crTitle `json:"title,omitempty"`
	Img   *crImg   `json:"img,omitempty"`
	Data  *crData  `json:"data,omitempty"`
}

type link struct {
	URL string `json:"url"`
}

type crTitle struct {
	Text string `json:"text,omitempty"`
}

type crImg struct {
	URL string `json:"url,omitempty"`
}

type crData struct {
	Value string `json:"value,omitempty"`
}

type eligibleCampaigns []campaign

// type geoRec struct {
// 	City struct {
// 		Names map[string]string `maxminddb:"names"`
// 	} `maxminddb:"city"`
// 	Subdivisions []struct {
// 		IsoCode string `maxminddb:"iso_code"`
// 	} `maxminddb:"subdivisions"`
// 	Country struct {
// 		IsoCode string `maxminddb:"iso_code"`
// 	} `maxminddb:"country"`
// }

type parsedUA struct {
	browser, os string
}

type newpub struct {
	ID       string   `json:"id"`
	Name     string   `json:"name"`
	Seats    []string `json:"seats"`
	Revshare float64  `json:"revshare"`
}

type newseat struct {
	ID    string `json:"id"`
	PubID string `json:"pubID"`
}

type modRevshare struct {
	ID       string  `json:"id"`
	Revshare float64 `json:"revshare"`
}

type ip2locRec struct {
	ToIP        []byte
	CountryCode string
	Region      string
	City        string
}

type recDecode struct {
	ToIP        big.Int
	CountryCode string
	Region      string
	City        string
}

type ipRecords struct {
	mu   sync.RWMutex
	Recs []ip2locRec
}

type appError struct {
	Error   error
	Message string
	Code    int
}

type appHandler func(http.ResponseWriter, *http.Request) *appError

func (fn appHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if e := fn(w, r); e != nil {
		fmt.Printf("%v\n%s\n", e.Error, e.Message)
		http.Error(w, e.Message, e.Code)
	}
}

//*****CheckFatalErr is for testing only. Replace with the commented-out error check above it for production.*****

func handleBid(w http.ResponseWriter, r *http.Request, p *uaparser.Parser, brs standardizeBidReqer, caf filterCampaigner) *appError {
	if r.Method == "POST" {
		campaignsMutex.RLock()
		creativesMutex.RLock()
		fieldsMutex.RLock()
		defer campaignsMutex.RUnlock()
		defer creativesMutex.RUnlock()
		defer fieldsMutex.RUnlock()

		u, err := url.Parse(r.URL.String())
		if err != nil {
			return &appError{err, "Error parsing url", http.StatusBadRequest}
		}

		q := u.Query()
		seatID := q["seat_id"][0]
		// sk := q["secret_key"][0]

		//User is for Fcap. Each seatID has a users set in AS.
		req, ip, user, country, region, city, pubRev, err := brs.StandardizeBidReq(bv, seatID, r.Body)
		if err != nil {
			return &appError{err, "Error standardizing bid request", http.StatusBadRequest}
		}

		ua, domain := req.Device.UA, req.Site.Domain
		//Bot checking will go here

		//For temp testing. Remove when bot checking is actually added
		if ip != "" {

		}

		//Geo data: Only geo data for the US will be accepted (assume UN/LOCODE).
		//All other regions will be gotten from maxmind via the IP address.
		//Will change if geo object in openrtb spec changes or if a standard
		//is agreed upon with all SSP's
		// var geoIPRec geoRec
		// country, div1, div2, city := "", "", "", ""
		// if geoFromIP {
		// 	parsedIP := net.ParseIP(ip)
		// 	err = db.Lookup(parsedIP, &geoIPRec)
		// 	CheckFatalErr(err)
		// 	country = geoIPRec.Country.IsoCode
		// 	city = geoIPRec.City.Names["en"]
		// 	for i, v := range geoIPRec.Subdivisions {
		// 		if i == 0 {
		// 			div1 = v.IsoCode
		// 		} else if i == 1 {
		// 			div2 = v.IsoCode
		// 		}
		// 	}
		// } else {
		// 	country = strings.ToUpper(req.Device.Geo.Country)
		// 	div1 = strings.ToUpper(req.Device.Geo.Region)
		// 	city = req.Device.Geo.City
		// }

		cat := "IAB24"
		//Check cat cache for domain to get category.
		//If domain does not exist in cat cache, send it to aerospike as IAB24,
		//then, using a udf, send the domain and its aerospike ID to adsGO to be categorized.
		//Update the cache from aerospike every 3 hours.

		os, browser := "", ""
		deviceType := req.Device.DeviceType
		uaMutex.Lock()

		//Move into func on interface that returns parsedUA object
		if _, exists := uaCache[ua]; exists {
			browser, os = uaCache[ua].browser, uaCache[ua].os
		} else {
			client := *p.Parse(ua)
			osFamily, osMajor, osMinor, osPatch := client.Os.Family, client.Os.Major, client.Os.Minor, client.Os.Patch
			browserFamily, browserMajor := client.UserAgent.Family, client.UserAgent.Major

			if osFamily == "Android" || osFamily == "iOS" || osFamily == "Mac OS X" {
				os = setDroid_iOS_Mac(ua, osFamily, osMajor, osMinor, osPatch, deviceType)
			} else if osFamily == "Windows Phone" {
				os = osFamily + "_" + osMajor + "_" + osMinor
			} else if strings.Contains(osFamily, "Windows") {
				os = osFamily
			} else if _, exists := distros[osFamily]; stringy.CaseInsensitiveStringContains(osFamily, "linux") || exists {
				os = "Linux"
			} else if stringy.CaseInsensitiveStringContains(osFamily, "blackberry") {
				os = "BlackBerry"
			} else if deviceType != 0 && deviceType != 1 {
				os = "Other_" + strconv.Itoa(deviceType)
			} else {
				os = "Other"
			}

			if browserFamily == "IE" {
				browser = browserFamily + "_" + browserMajor
			} else if _, exists := fields.Browser[browserFamily]; exists {
				browser = browserFamily
			} else {
				browser = "Other"
			}

			uaCache[ua] = parsedUA{browser, os}
		}
		uaMutex.Unlock()

		//Campaign ID's that match the req's country, os, or browser
		matches := map[string]map[int]bool{
			"os":      fields.OS[os],
			"browser": fields.Browser[browser],
			"country": fields.Country[country],
		}

		//Find shortest map of matching campaign ID's
		key, shortest := "country", matches["country"]
		if len(matches["browser"]) < len(shortest) {
			key, shortest = "browser", matches["browser"]
		}
		if len(matches["os"]) < len(shortest) {
			key, shortest = "os", matches["os"]
		}

		//Campaigns that match all requirements in matches map
		var allPassed eligibleCampaigns
		var passedDesc eligibleCampaigns
		//Filter campaigns by fields in matches map
		for id := range shortest {
			count := 0
			for k, v := range matches {
				//Skip key for ID's slice being iterated through
				if k != key {
					if _, exists := v[id]; exists {
						count++
					}
				}
			}
			if count == len(matches)-1 {
				if campaigns[id].HasDesc {
					passedDesc = append(passedDesc, campaigns[id])
				}
				allPassed = append(allPassed, campaigns[id])
			}
		}

		if len(allPassed) < 1 {
			return &appError{fmt.Errorf("allPassed is empty"), "No eligible campaigns", http.StatusNoContent}
		}

		//Sort passing campaigns by CPM
		sort.Sort(sort.Reverse(allPassed))
		sort.Sort(sort.Reverse(passedDesc))

		bids := caf.FilterCampaigns(av, crf, req, allPassed, passedDesc, req.Badv, pubRev, deviceType, domain, cat, country, region, city, user)
		if len(bids) < 1 {
			return &appError{fmt.Errorf("bids is empty"), "No passed campaigns", http.StatusNoContent}
		}

		seat := seatBid{Bid: bids, Seat: seatID}
		res := bidRes{ID: req.ID, SeatBid: []seatBid{seat}}
		if err = bidResMaker(seatID, &res); err != nil {
			return &appError{err, fmt.Sprintf("Error creating custom bid response for seatID %s", seatID), http.StatusNoContent}
		}

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)

		if err = json.NewEncoder(w).Encode(res); err != nil {
			log.Printf("Error during json encoding response: %v\n", err)
		}
		return nil
	}
	return &appError{methodErr, "Non-POST HTTP method used for bid request", http.StatusMethodNotAllowed}
}

type filterCampaigner interface {
	FilterCampaigns(av validateAsseter, crf filterCreativer, req bidReq, allPassed, passedDesc eligibleCampaigns, badv []string, pubRev float64, deviceType int, domain, cat, country, region, city, user string) []bid
}

type campaignFilter struct{}

//Put in interface that returns []bid
func (caf *campaignFilter) FilterCampaigns(av validateAsseter, crf filterCreativer, req bidReq, allPassed, passedDesc eligibleCampaigns, badv []string, pubRev float64, deviceType int, domain, cat, country, region, city, user string) []bid {
	var bids []bid
	for i, l := 0, len(req.Imp); i < l; i++ {
		var passed eligibleCampaigns
		nextImp, desc2, descLen := av.ValidateAssets(req.Imp[i])
		if nextImp {
			continue
		} else if descLen > -1 || desc2 {
			passed = passedDesc
		} else {
			passed = allPassed
		}

		//req.ID, impID, bidID, and crid will be combined to make nurl
		impID := req.Imp[i].ID
		bidFloor := req.Imp[i].BidFloor
		pc := req.Imp[i].Plcmtcnt
		if pc < 1 {
			pc = 1
		}

		for ; pc > 0; pc-- {
			bidID := genid.RandStringID(64)
			for ii, ll := 0, len(passed); ii < ll; ii++ {
				passedCamp := passed[ii]
				bidPrice := passedCamp.CPM * pubRev

				//Separate call to aerospike with user string to check Fcap. Continue if Fcap is met for that user on a campaign
				fmt.Println(user)

				if bidFloor > bidPrice {
					pc = 1 // Stop outer pc loop
					break
				}
				//Check flow rate
				if rand.Intn(100)+1 > passedCamp.Flow {
					continue
				}
				//Check deviceType if campaign targets deviceTypes 3, 6, or 7
				if len(passedCamp.DeviceType) > 0 && deviceType == 3 || deviceType == 6 || deviceType == 7 {
					if _, exists := passedCamp.DeviceType[deviceType]; !exists {
						continue
					}
				}
				//Check campaign blacklist and whitelist for req domain
				if len(passedCamp.Bpub) > 0 {
					if _, exists := passedCamp.Bpub[domain]; exists {
						continue
					}
				} else if len(passedCamp.Wpub) > 0 {
					if _, exists := passedCamp.Wpub[domain]; !exists {
						continue
					}
				}
				//Check campaign categories for req category
				if len(passedCamp.Cat) > 0 {
					if _, exists := passedCamp.Cat[cat]; !exists {
						continue
					}
				}

				//Check if req passes geography requirements of campaign
				switch {
				case passedCamp.Geo[country][region][city]:
				case len(passedCamp.Geo[country][region]) > 0:
				case len(passedCamp.Geo[country]) > 0:
				default:
					continue
				}

				//Unknown if cats will be assigned to campaigns or creatives
				//If on campaign, check here.
				//If on creative, check in creativeFilter

				//ID's of qualified creatives for an impression. Will be randomly chosen from.
				crids := map[int]struct{}{}
				for k, v := range passedCamp.Domains {
					if blocked := isBadv(k, badv); !blocked {
						for crid := range v {
							crids[crid] = struct{}{}
						}
					}
				}

				if len(crids) < 1 {
					continue
				}

				crAssets, dest, crid := crf.FilterCreatives(af, descLen, passedCamp.ID, desc2, crids, req.Imp[i].Assets)
				if len(crAssets) < 1 || dest == "" {
					continue
				}
				admObj := adm{Assets: crAssets, Link: link{URL: dest}}
				b, err := json.Marshal(&admObj)
				CheckFatalErr(err)
				admStr := string(b)
				bidObj := bid{
					ID:    bidID,
					ImpID: impID,
					Price: bidPrice,
					//Add cpm, pubcpm, and campaignID
					NURL: "45.55.157.222:3001/win?req_id=" + req.ID + "&bid_id=" + bidID + "&imp_id=" + impID + "&crid=" + strconv.Itoa(crid) + "&adv_id=" + strconv.Itoa(passedCamp.AdvID) + "&price=${AUCTION_PRICE}", //temporary
					ADM:  admStr,
				}
				bids = append(bids, bidObj)
			}
		}
	}
	return bids
}

type filterCreativer interface {
	FilterCreatives(af filterAsseter, descLen, cid int, desc2 bool, crids map[int]struct{}, impAssets []impAsset) ([]crAsset, string, int)
}

type creativeFilter struct{}

//Put on own interface that returns ([]crAsset, string, int)
func (crf *creativeFilter) FilterCreatives(af filterAsseter, descLen, cid int, desc2 bool, crids map[int]struct{}, impAssets []impAsset) ([]crAsset, string, int) {
	count := 0
	//Get number of imps for the campaign
	campaignImpsKey, _ := as.NewKey("count", "campaignImps", cid)
	campaignImps, err := client.Get(policy, campaignImpsKey)
	if err != nil {
		//create errLog object to save in Aerospike
		return []crAsset{}, "", 0
	}

	//Get imp numbers for each creative
	keys := make([]int, len(crids))
	creativeImpsKeys := make([]*as.Key, len(crids))
	for k := range crids {
		keys[count] = k
		creativeImpsKeys[count], _ = as.NewKey("count", "creativeImps", k)
		count++
	}
	creativeImps, err := client.BatchGet(policy, creativeImpsKeys)
	if err != nil {
		//create errLog object to save in Aerospike
		return []crAsset{}, "", 0
	}

	//Check if any creatives have an imp count > 1000
	enoughImps := false
	for i, j := 0, len(creativeImps); i < j; i++ {
		if creativeImps[i].count >= 1000 {
			enoughImp = true
			break
		}
	}

	//Select creatives with weighted probabilty based on imps and CTR
	if enoughImps || campaignImps.count >= 10000 {
		count = 0
		creativeClicksKeys := make([]*as.Key, len(crids))
		for k := range crids {
			creativeClicksKeys[count], _ = as.NewKey("count", "creativeClicks", k)
			count++
		}
		creativeClicks, err := client.BatchGet(policy, creativeClicksKeys)
		if err != nil {
			//create errLog object to save in Aerospike
			return []crAsset{}, "", 0
		}
		keys = creativeCDF(creativeImps, creativeClicks)
		for i, j := 0, len(keys); i < j; i++ {
			cr := creatives[keys[i]]
			crAssets, err := af.FilterAssets(cr, impAssets, desc2, descLen)
			if err == nil {
				return crAssets, cr.Dest, cr.ID
			}
		}
	} else {
		l := len(keys)
		r := rand.Perm(l)

		for i := 0; i < l; i++ {
			cr := creatives[keys[r[i]]]
			crAssets, err := af.FilterAssets(cr, impAssets, desc2, descLen)
			if err == nil {
				return crAssets, cr.Dest, cr.ID
			}
		}
	}

	return []crAsset{}, "", 0
}

type filterAsseter interface {
	FilterAssets(cr creative, impAssets []impAsset, desc2 bool, descLen int) ([]crAsset, error)
}

type assetFilter struct{}

//Put on own interface that returns ([]crAsset, error)
func (af *assetFilter) FilterAssets(cr creative, impAssets []impAsset, desc2 bool, descLen int) ([]crAsset, error) {
	var crAssets []crAsset
	descStr1, descStr2 := "", ""
	for i, l := 0, len(impAssets); i < l; i++ {
		ia := impAssets[i]

		if ia.ID == nil {
			if ia.Required == 1 {
				return []crAsset{}, fmt.Errorf("Required impression asset missing ID")
			}
			continue

		} else if ia.Img != nil {
			crAssets = append(crAssets, crAsset{ID: *ia.ID, Img: &crImg{URL: cr.Img}})

		} else if ia.Title != nil {
			crAssets = append(crAssets, crAsset{ID: *ia.ID, Title: &crTitle{Text: stringy.TruncateEllipsis(cr.Title, ia.Title.Len)}})

		} else if ia.Data != nil {
			switch ia.Data.Type {
			case 1:
				crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: stringy.TruncateEllipsis(cr.Display, ia.Data.Len)}})
			case 2:
				if cr.Desc != "" {
					if descStr1 == "" {
						if desc2 && cr.Desc2 == "" {
							//If we split up the creative's description based the required lengths of desc
							//and desc2, the two descriptions could end up being very different lengths.
							descStr1, descStr2 = stringy.Halves(stringy.TruncateEllipsis(cr.Desc, ia.Data.Len))
						} else {
							descStr1 = stringy.TruncateEllipsis(cr.Desc, ia.Data.Len)
						}
					}
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: descStr1}})
				}
			case 3:
				if cr.Rating != "" {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: cr.Rating}})
				} else if ia.Required == 1 {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: string(rand.Intn(11))}})
				}
			case 4:
				if cr.Likes != "" {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: cr.Likes}})
				} else if ia.Required == 1 {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: string(rand.Intn(274873) + 716298)}}) //716298-991170 likes
				}
			case 5:
				if cr.Downloads != "" {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: cr.Downloads}})
				} else if ia.Required == 1 {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: string(rand.Intn(635685) + 748353)}}) //748353-1384037 downloads
				}
			case 10:
				if cr.Desc2 != "" {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: stringy.TruncateEllipsis(cr.Desc2, ia.Data.Len)}})
				} else if ia.Required == 1 {
					if descStr2 == "" {
						if descLen > -1 {
							descStr1, descStr2 = stringy.Halves(stringy.TruncateEllipsis(cr.Desc, descLen))
						} else { //Only if desc2 is required and desc isn't, which is unlikely
							descStr2 = stringy.TruncateEllipsis(cr.Desc, ia.Data.Len)
						}
					}
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: stringy.TruncateEllipsis(descStr2, ia.Data.Len)}})
				}
			case 11:
				if cr.DisplayURL != "" {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: stringy.TruncateEllipsis(cr.DisplayURL, ia.Data.Len)}})
				} else if ia.Required == 1 {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: stringy.TruncateEllipsis(cr.Display, ia.Data.Len)}})
				}
			case 12:
				if cr.C2A != "" {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: stringy.TruncateEllipsis(cr.C2A, ia.Data.Len)}})
				} else if ia.Required == 1 {
					crAssets = append(crAssets, crAsset{ID: *ia.ID, Data: &crData{Value: "Learn More"}})
				}
			default:
				if ia.Required == 1 {
					return []crAsset{}, fmt.Errorf("Creative cannot fulfill a required asset of impression")
				}
			}
		} else if ia.Required == 1 {
			return []crAsset{}, fmt.Errorf("Creative cannot fulfill a required asset of impression")
		}
	}
	return crAssets, nil
}

type validateAsseter interface {
	ValidateAssets(imp imp) (bool, bool, int)
}

type assetValidator struct{}

//Put on own interface that returns (bool, bool, int)
func (av *assetValidator) ValidateAssets(imp imp) (bool, bool, int) {
	desc2, descLen := false, -1
	//Unsupported data asset types
	uat := map[int]struct{}{
		6: struct{}{},
		7: struct{}{},
		8: struct{}{},
		9: struct{}{},
	}
	if imp.ID == "" {
		return true, false, -1
	}
	for a, b := 0, len(imp.Assets); a < b; a++ {
		v := imp.Assets[a].Video
		r := imp.Assets[a].Required
		d := imp.Assets[a].Data
		t, l := 0, 0
		if d != nil {
			t, l = d.Type, d.Len
		}
		if r == 1 {
			if _, exists := uat[t]; exists || v != nil {
				return true, false, -1
			} else if t == 2 {
				descLen = l
			} else if t == 10 {
				desc2 = true
			}
		}
	}
	return false, desc2, descLen
}

func addPub(w http.ResponseWriter, r *http.Request) *appError {
	if r.Method == "POST" {
		var p newpub
		err := json.NewDecoder(r.Body).Decode(&p)

		publishersMutex.Lock()
		seatsMutex.Lock()
		defer publishersMutex.Unlock()
		defer seatsMutex.Unlock()

		if err != nil {
			return &appError{err, "Invalid Request: Need publisher ID and name, array of seat IDs, and revenue share.", http.StatusBadRequest}
		} else if p.ID == "" {
			return &appError{err, "Invalid Request: Empty publisher ID.", http.StatusBadRequest}
		} else if _, exists := publishers[p.ID]; exists {
			return &appError{err, "Publisher ID already exists. Seats will not be added.", http.StatusBadRequest}
		} else if p.Name == "" {
			return &appError{err, "Invalid Request: Empty publisher name.", http.StatusBadRequest}
		} else if p.Revshare <= 0.0 {
			return &appError{err, "Invalid Request: Revshare must be greater than 0.", http.StatusBadRequest}
		}

		for i, j := 0, len(p.Seats); i < j; i++ {
			if p.Seats[i] == "" {
				return &appError{err, "Invalid Request: Empty seat ID. Publisher and seats will not be added.", http.StatusBadRequest}
			} else if _, exists := seats[p.Seats[i]]; exists {
				return &appError{err, fmt.Sprintf("Seat ID %s already exists. Publisher and seats will not be added.", p.Seats[i]), http.StatusBadRequest}
			}
			//Add seatIDs after all have been proven to not exist
			if i+1 == j {
				for k := i; k >= 0; k-- {
					seats[p.Seats[k]] = p.ID
				}
			}

		}
		publishers[p.ID] = pub{Name: p.Name, Revshare: p.Revshare}

		w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		return nil
	}
	return &appError{methodErr, "Invalid Request: Must be POST request.", http.StatusBadRequest}
}

func addSeat(w http.ResponseWriter, r *http.Request) *appError {
	if r.Method == "POST" {
		var s newseat
		err := json.NewDecoder(r.Body).Decode(&s)

		publishersMutex.RLock()
		seatsMutex.Lock()
		defer publishersMutex.RUnlock()
		defer seatsMutex.Unlock()

		if err != nil {
			return &appError{err, "Invalid Request: Need seat ID and publisher ID.", http.StatusBadRequest}
		} else if s.PubID == "" {
			return &appError{err, "Invalid Request: Empty publisher ID.", http.StatusBadRequest}
		} else if _, exists := publishers[s.PubID]; !exists {
			return &appError{err, fmt.Sprintf("Publisher ID %s does not exists. Use addPub route to add a new publisher first.", s.PubID), http.StatusBadRequest}
		} else if s.ID == "" {
			return &appError{err, "Invalid Request: Empty seat ID.", http.StatusBadRequest}
		} else if _, exists := seats[s.ID]; exists {
			return &appError{err, fmt.Sprintf("Seat ID %s already exists.", s.ID), http.StatusBadRequest}
		}
		seats[s.ID] = s.PubID

		w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		return nil
	}
	return &appError{methodErr, "Invalid Request: Must be POST request.", http.StatusBadRequest}
}

func updateRevshare(w http.ResponseWriter, r *http.Request) *appError {
	if r.Method == "POST" {
		var m modRevshare
		err := json.NewDecoder(r.Body).Decode(&m)

		publishersMutex.Lock()
		defer publishersMutex.Unlock()

		if err != nil {
			return &appError{err, "Invalid Request: Need publisher ID and revenue share.", http.StatusBadRequest}
		} else if m.ID == "" {
			return &appError{err, "Invalid Request: Empty publisher ID.", http.StatusBadRequest}
		} else if _, exists := publishers[m.ID]; !exists {
			return &appError{err, fmt.Sprintf("Publisher ID %s does not exists. Use addPub route to add a new publisher first.", m.ID), http.StatusBadRequest}
		} else if m.Revshare <= 0.0 {
			return &appError{err, "Invalid Request: Revshare must be greater than 0.", http.StatusBadRequest}
		}
		//https://code.google.com/p/go/issues/detail?id=3117
		p := publishers[m.ID]
		p.Revshare = m.Revshare
		publishers[m.ID] = p

		w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		return nil
	}
	return &appError{methodErr, "Invalid Request: Must be POST request.", http.StatusBadRequest}
}

func setDroid_iOS_Mac(ua, osFamily, osMajor, osMinor, osPatch string, deviceType int) string {
	os := ""
	if osMinor != "" {
		if osPatch != "" && osFamily != "Mac OS X" {
			os = osFamily + "_" + osMajor + "_" + osMinor + "_" + osPatch
		} else {
			os = osFamily + "_" + osMajor + "_" + osMinor
		}
	} else {
		os = osFamily
	}
	if osFamily == "iOS" {
		if stringy.CaseInsensitiveStringContains(ua, "ipad") {
			os += "_" + "ipad"
		} else if stringy.CaseInsensitiveStringContains(ua, "ipod") {
			os += "_" + "ipod"
		} else {
			os += "_" + "iphone"
		}
	} else if osFamily == "Android" {
		osMajorInt, _ := strconv.Atoi(osMajor)
		if deviceType == 5 || osMajorInt > 3 && !stringy.CaseInsensitiveStringContains(ua, "mobile") {
			os += "_" + "tablet"
		} else {
			os += "_" + "phone"
		}
	}
	return os
}

func isBadv(s string, ss []string) bool {
	s = strings.ToLower(s)
	for i, j := 0, len(ss); i < j; i++ {
		if strings.ToLower(ss[i]) == s {
			return true
		}
	}
	return false
}

// func isBadv(s string, ss []string) bool {
// 	for i, j := 0, len(ss); i < j; i++ {
// 		if strings.ToUpper(strings.Split(ss[i], "-")[0]) == s {
// 			return true
// 		}
// 	}
// 	return false
// }

// func badReq(w http.ResponseWriter, body string) {
// 	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
// 	w.WriteHeader(http.StatusBadRequest)
// 	w.Write([]byte(body))
// 	return
// }

// func noBid(w http.ResponseWriter) {
// 	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
// 	w.WriteHeader(http.StatusNoContent)
// 	return
// }

//Sorting methods to find qualified campaigns with highest CPM
func (slice eligibleCampaigns) Len() int {
	return len(slice)
}

func (slice eligibleCampaigns) Less(i, j int) bool {
	return slice[i].CPM < slice[j].CPM
}

func (slice eligibleCampaigns) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

//For production, change fetch IP to the parsing server
func updateIP2Loc(w http.ResponseWriter, r *http.Request) *appError {
	if err := fetch("http://127.0.0.1:3000"); err != nil {
		return &appError{err, "Error fetching IP2Location cache from parser", 404}
	} else {
		fmt.Println(ipRecs.Recs[28])
		w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
	}
	return nil
}

func fetch(url string) error {
	timeout := time.Duration(180 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	res, err := client.Get(url)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	l, err := strconv.Atoi(res.Header.Get("Recs-Length"))
	if err != nil {
		return err
	}

	t := make([]ip2locRec, l)
	dec := json.NewDecoder(res.Body)
	r := recDecode{}
	for i := 0; i < l; i++ {

		err = dec.Decode(&r)
		if err != nil {
			fmt.Println("Could not parse object")
			return err
		}

		t[i] = ip2locRec{
			ToIP:        r.ToIP.Bytes(),
			CountryCode: r.CountryCode,
			Region:      r.Region,
			City:        r.City,
		}
	}

	ipRecs.mu.Lock()
	ipRecs.Recs = t
	ipRecs.mu.Unlock()
	t = nil
	return nil
}

func main() {
	// runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UnixNano())

	log.Println("Ready")

	parser, err := uaparser.New("regexes.yaml")
	CheckFatalErr(err)
	// db, err := maxminddb.Open("GeoIP2-City.mmdb")
	// CheckFatalErr(err)

	http.HandleFunc("/bid", func(w http.ResponseWriter, r *http.Request) {
		if e := handleBid(w, r, parser, brs, caf); e != nil {
			fmt.Printf("%v\n%s\n", e.Error, e.Message)
			http.Error(w, "", e.Code)
		}
	})

	http.Handle("/newpub", appHandler(addPub))
	http.Handle("/newseat", appHandler(addSeat))
	http.Handle("/revshare", appHandler(updateRevshare))
	http.Handle("/updateIP2Loc", appHandler(updateIP2Loc))

	http.ListenAndServe(serverPort, nil)
}
